import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Configuration } from './core/providers/configuration';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [],
})
export class AppComponent {
  title = 'TODOS';
  constructor(cfg: Configuration, i18n: TranslateService) {
    i18n.get('welcome').subscribe((text) => {
      console.log(text);
    });
    console.log(cfg.get('strapi'));
  }
}

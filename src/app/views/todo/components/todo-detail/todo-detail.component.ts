import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { Todo } from 'src/app/core/models/Todo';
import { TodoService } from '../../providers/todo.service';

@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styles: [],
  animations: [
    trigger('redGreen', [
      state(
        'green',
        style({
          'background-color': 'green',
        }),
      ),
      state(
        'red',
        style({
          'background-color': 'red',
        }),
      ),
      transition('green => red', [animate('1s')]),
      transition('red => green', [animate('200ms')]),
    ]),
  ],
})
export class TodoDetailComponent implements OnInit {
  public todo$: Observable<Todo>;

  public state = 'green';

  public boxColor = 'red';

  constructor(route: ActivatedRoute, service: TodoService) {
    this.todo$ = route.paramMap.pipe(
      map((params) => params.get('id')),
      switchMap((id) => service.findOne(+id)),
    );
  }

  ngOnInit(): void {}
  clicked(e: string) {
    console.log('received event with color', e);
  }
}

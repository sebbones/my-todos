import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { Todo } from 'src/app/core/models/Todo';
import { TodoService } from '../../providers/todo.service';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styles: [
    ':host { display: flex; flex-direction: column } .red { color: red }',
  ],
})
export class TodoListComponent implements OnInit {
  public todos$: Observable<Todo[]>;
  public isOnlyUnchecked$ = new Subject();

  public searchControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  public onlyUncheckedControl = new FormControl();

  constructor(
    public service: TodoService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.todos$ = combineLatest([
      this.searchControl.valueChanges.pipe(
        debounceTime(300),
        startWith(''),
        distinctUntilChanged(),
      ),
      this.route.queryParamMap.pipe(
        map((params) => !!params.get('only-unchecked')),
        tap((onlyUnchecked) => {
          this.onlyUncheckedControl.setValue(onlyUnchecked);
        }),
      ),
    ]).pipe(
      switchMap(([search, onlyUnchecked]) => {
        console.log(search, onlyUnchecked);
        return this.service.find(search, onlyUnchecked);
      }),
    );

    this.onlyUncheckedControl.valueChanges.subscribe((value) => {
      this.setOnlyUnchanged(value);
    });
  }

  setOnlyUnchanged(e: MatSlideToggleChange) {
    if (e.checked) {
      this.router.navigate([], {
        queryParams: {
          'only-unchecked': 'true',
        },
      });
    } else {
      this.router.navigate([]);
    }
  }

  toggleCheck(todo: Todo) {
    this.service
      .patch(todo.id, {
        checked: !todo.checked,
      })
      .subscribe((newTodo) => {
        todo.checked = newTodo.checked;
      });
  }

  ngOnInit(): void {}
}

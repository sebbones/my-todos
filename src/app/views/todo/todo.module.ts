import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TodoRoutingModule } from './todo-routing.module';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoDetailComponent } from './components/todo-detail/todo-detail.component';
import { TodoComponent } from './components/todo/todo.component';
import { TodoService } from './providers/todo.service';

import { MatListModule } from '@angular/material/list';
import { TranslateModule } from '@ngx-translate/core';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/modules/shared/shared.module';

@NgModule({
  declarations: [TodoListComponent, TodoDetailComponent, TodoComponent],
  imports: [
    CommonModule,
    TodoRoutingModule,
    MatListModule,
    TranslateModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
    SharedModule,
  ],
  providers: [TodoService],
})
export class TodoModule {}

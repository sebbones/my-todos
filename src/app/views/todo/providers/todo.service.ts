import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Todo } from 'src/app/core/models/Todo';
import { Configuration } from 'src/app/core/providers/configuration';

@Injectable()
export class TodoService {
  private strapiBase: string;

  constructor(private http: HttpClient, private cfg: Configuration) {
    this.strapiBase = this.cfg.get('strapi');
  }

  // find multiple with params
  find(query?: string, onlyUnchecked?: boolean) {
    const params = {};
    if (query) {
      params['title_contains'] = query;
    }
    if (onlyUnchecked) {
      params['checked_eq'] = 'true';
    }
    return this.http.get<Todo[]>(this.strapiBase + '/todos', {
      params,
    });
  }

  // find single with params
  findOne(id: number) {
    return this.http.get<Todo>(this.strapiBase + '/todos/' + id);
  }

  // patch single
  patch(id: number, delta: Partial<Todo>) {
    return this.http.put<Todo>(this.strapiBase + '/todos/' + id, delta);
  }
  update() {}

  create() {}

  delete() {}
}

import { APP_INITIALIZER } from '@angular/core';
import { NgModule } from '@angular/core';
import { Configuration } from './providers/configuration';

export function configFactory(cfg: Configuration) {
  return () => cfg.load();
}

@NgModule({
  imports: [],
  providers: [
    Configuration,
    {
      provide: APP_INITIALIZER,
      useFactory: configFactory,
      deps: [Configuration],
      multi: true,
    },
  ],
})
export class CoreModule {}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class Configuration {
  private configuration: any;
  constructor(private http: HttpClient) {}
  public async load() {
    return this.http
      .get(environment.cfgURL)
      .toPromise()
      .then((config) => {
        this.configuration = config;
      });
  }
  public get<T>(name: string): T {
    return this.configuration[name] as T;
  }
}

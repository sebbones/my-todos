import { User } from './User';

export interface Todo {
  id: number;
  title: string;
  checked: boolean;
  user: User;
}

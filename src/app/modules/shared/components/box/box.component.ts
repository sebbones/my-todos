import {
  Component,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styles: [':host { display: block; width: 100px; height: 100px } '],
})
export class BoxComponent implements OnInit {
  @HostBinding('style.background-color') public backgroundColor;

  private _color: string;
  @Input()
  public set color(value: string) {
    this.backgroundColor = value;
    this._color = value;
  }
  public get color() {
    return this._color;
  }

  @HostListener('click', ['$event'])
  clicked(e) {
    this.myClick.emit(this.backgroundColor);
  }

  @Output()
  public myClick = new EventEmitter<string>();

  constructor() {
    this.myClick.subscribe(() => {
      console.log('emitted click event');
    });
  }

  ngOnInit(): void {}
}
